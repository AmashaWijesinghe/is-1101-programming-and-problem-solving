#include<stdio.h>

struct student{
	char name[100], subject[10];
	float marks;
};

int main()
{
	struct student stu[100];
	int i,n;
	printf("Enter the number of students:");
	scanf("%d", &n);
	printf("Enter the information.\n");
	
	for(i=0; i<n; i++)
	{
	printf("Enter the name of student %d:\n", i+1);
	scanf("%s", stu[i].name);
	printf("Enter the subject:\n");
	scanf("%s", stu[i].subject);
	printf("Enter the marks of student %d:\n", i+1);
	scanf("%f", &stu[i].marks);
	}
	printf("\n");

	printf("Display information.\n");

	for(i=0; i<n; i++)
	{
		printf("%d.\n", i+1);
		printf("Name: %s\n", stu[i].name);
		printf("Subject: %s\n", stu[i].subject);
		printf("Marks: %.1f\n", stu[i].marks);
		printf("\n");
	}
	printf("\n");
	return 0;
}




