FROM gcc
MAINTAINER Amasha Wijesinghe
RUN apt-get update && apt-get install -y \
vim \
gdb \
git
RUN git config --global user.email "amashawijesinghe1999@gmail.com"
RUN git config --global user.name "AmashaWijesinghe"
